<?php
/* Template Name: About Us */

get_header();
?>

  <!-- catg header banner section -->
  <section id="aa-catg-head-banner">
   <img src="<?php echo get_template_directory_uri() ?>/assets/images/fashion/fashion-header-bg-8.jpg" alt="fashion img">
   <div class="aa-catg-head-banner-area">
     <div class="container">
      <div class="aa-catg-head-banner-content">
        <h2><?php the_title() ?></h2>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>         
          <li class="active"><?php the_title() ?></li>
        </ol>
      </div>
     </div>
   </div>
  </section>
  <!-- / catg header banner section -->

  
  <!-- Blog Archive -->
  <section id="aa-blog-archive">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-blog-archive-area">
            <div class="row">
              <div class="col-md-9">
                <!-- Blog details -->
                <div class="aa-blog-content aa-blog-details">
                  <article class="aa-blog-content-single">                        
                    <h2><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem, praesentium?</a></h2>
                     <div class="aa-article-bottom">
                      <div class="aa-post-author">
                        Posted By <a href="#">Jackson</a>
                      </div>
                      <div class="aa-post-date">
                        March 26th 2016
                      </div>
                    </div>
                    <figure class="aa-blog-img">
                      <a href="#"><img src="<?php echo get_template_directory_uri() ?>/assets/images/fashion/3.jpg" alt="fashion img"></a>
                    </figure>
					
					
					<div class="blog-single-bottom">
                      <div class="row">
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <div class="blog-single-tag">
                            <span>Tags:</span>
                            <a href="#">Fashion,</a>
                            <a href="#">Beauty,</a>
                            <a href="#">Lifestyle</a>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <div class="blog-single-social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                          </div>
                        </div>
                      </div>
                    </div>
                   
                  </article>
                </div>
              </div>

              <!-- blog sidebar -->
			<div class="col-lg-3 col-md-3">
          		<aside class="aa-sidebar">
					<?php if ( is_active_sidebar( 'shop_sidebar' ) ) { ?>
						<div id="shop_sidebar">
							<?php dynamic_sidebar('shop_sidebar'); ?>
						</div>
					<?php } ?>
				<!-- single sidebar --
				<div class="aa-sidebar-widget">
				<h3>Category</h3>
              
             <?php 
                $orderby = 'name';
                $order = 'asc';
                $hide_empty = false ;
                $cat_args = array(
                    'orderby'    => $orderby,
                    'order'      => $order,
                    'hide_empty' => $hide_empty,
                );
                
                $product_categories = get_terms( 'product_cat', $cat_args );
                
                if( !empty($product_categories) ){
                    echo '<ul  class="aa-catg-nav">';
                    foreach ($product_categories as $key => $category) {
                      if(strtolower($category->name) != 'uncategorized'){
                        echo '<li>';
                          echo '<a href="'.get_term_link($category).'" >';
                            echo ucwords($category->name);
                          echo '</a>';
                        echo '</li>';

                      }
                    }
                    echo '</ul>';
                }
              ?>
              
            </div>
            -- single sidebar --
            <div class="aa-sidebar-widget">
              <h3>Tags</h3>
              <div class="tag-cloud">
                
               <?php
                  $product_tags = get_terms( 'product_tag' );
                  //only start if we have some tags
                  if ( $product_tags && ! is_wp_error( $product_tags ) ) { 
                    foreach ($product_tags as $tag) {
                      $tag_title = $tag->name; // tag name
                      $tag_link = get_term_link( $tag );// tag archive link
                  
                      echo '<a  href="'.$tag_link.'">'.ucwords($tag_title).' </a>';
                    }
                  } 
               ?>
              </div>
            </div>
            -- single sidebar --
            <div class="aa-sidebar-widget">
              <h3>Shop By Price</h3>              
              !-- price range --
              <div class="aa-sidebar-price-range">
               <form action="">
                  <div id="skipstep" class="noUi-target noUi-ltr noUi-horizontal noUi-background">
                  </div>
                  <span id="skip-value-lower" class="example-val">30.00</span>
                 <span id="skip-value-upper" class="example-val">100.00</span>
                 <button class="aa-filter-btn" type="submit">Filter</button>
               </form>
              </div>              

            </div--->
            <!-- single sidebar --
            <div class="aa-sidebar-widget">
              <h3>Shop By Color</h3>
              <div class="aa-color-tag">
                <a class="aa-color-green" href="#"></a>
                <a class="aa-color-yellow" href="#"></a>
               
              </div>                            
            </div>
            -- single sidebar --
            <div class="aa-sidebar-widget">
              <h3>Recently Views</h3>
              <div class="aa-recently-views">
                <ul>
                  <li>
                    <a href="#" class="aa-cartbox-img"><img alt="img" src="<?php echo get_template_directory_uri() ?>/assets/images/woman-small-2.jpg"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">Product Name</a></h4>
                      <p>1 x $250</p>
                    </div>                    
                  </li>
                                                   
                </ul>
              </div>                            
            </div>
            -- single sidebar --
            <div class="aa-sidebar-widget">
              <h3>Top Rated Products</h3>
              <div class="aa-recently-views">
                <ul>
                  <li>
                    <a href="#" class="aa-cartbox-img"><img alt="img" src="<?php echo get_template_directory_uri() ?>/assets/images/woman-small-2.jpg"></a>
                    <div class="aa-cartbox-info">
                      <h4><a href="#">Product Name</a></h4>
                      <p>1 x $250</p>
                    </div>                    
                  </li>
                                       
                </ul>
              </div>                            
            </div>---->
          </aside>
      </div>
            </div>           
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Blog Archive -->


  <!-- Subscribe section -->
  <section id="aa-subscribe">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-subscribe-area">
            <h3>Subscribe our newsletter </h3>
            <p>Stylista is for you</p>
            <form action="" class="aa-subscribe-form">
              <input type="email" name="" id="" placeholder="Enter your Email">
              <input type="submit" value="Subscribe">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Subscribe section -->

<?php
get_footer();
